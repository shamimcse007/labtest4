<?php
include_once ('../../../../vendor/autoload.php');
use App\Seip\Id158554\Mobile\Mobile;
$obj= new Mobile();
$itemPerPage=5;
if(!empty($_GET['page'])){
 $pageNumber = $_GET['page'];
}else{
    $pageNumber=0;
}
if(!empty($_GET['page'])){
    $offset = $itemPerPage * $_GET['page'];
}else{
    $offset = 0;
}
$allData =$obj->index($itemPerPage,$offset);
$totalRows=$allData['totalRow'];
$numberOfPage=ceil($totalRows/$itemPerPage);
echo 'number of page:'.$numberOfPage; echo '<br>';
echo 'total Rows:'.$totalRows;
array_pop($allData);
?>
<?php
if(isset($_SESSION['message'])){
    echo $_SESSION['message'];
    unset($_SESSION['message']);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>labtest 04</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body style="">
<span id="utility">Download as <a href="pdf.php">PDF</a></span>
<span id="utility">Download as <a href="xl.php">XL File</a></span>
<form action="search.php" method="get">
    <input type="text" name="keyword" />
    <input type="submit" value="Go !">
</form>
<table class="table table-bordered" style="width: 800px;margin:0 auto; position:relative; ">
<h3> <button style="margin: 0 auto"><a href="create.php">Add Mobile Data</a> </button></h3>
    <thead>
    <tr>
        <th>Serial</th>
        <th>ID</th>
        <th>Mobile_Model</th>
        <th>Mobile_Name</th>
        <th>mobile_price</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
<?php
$serial=$itemPerPage * $pageNumber + 1;
foreach($allData as $key => $value) { ?>
    <tr>
            <td><?php echo $serial++?></td>
            <td><?php echo $value['id']?></td>
            <td><?php echo $value['mobile_model']?></td>
            <td><?php echo $value['mobile_name']?></td>
            <td><?php echo $value['mobile_price']?></td>

    </tr>

<?php  } ?>
    </tbody>
<button>
    <?php
    for($i=0; $i<$numberOfPage; $i++)
    {
        echo "<a href=index.php?page=$i>" . $i .","."</a>";
    }
    ?>
</button>
</table>

</body>
</html>
